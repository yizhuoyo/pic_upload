function saveremark(picId) {
    var remark = $("#picId"+picId+" textarea").val();
    var originremark = $("#picId"+picId+" textarea").attr("title");
    //如果并未修改内容
    if (remark==originremark)
    {
        return;
    }

    $.ajax({
        url:"/editremark/"+picId,
        cache:false,
        //async: false,
        data:{remark:remark},
        type:"post",
        success:function (msg) {
            //判断修改成功或失败
            layer.msg(msg);
            if (msg==="修改成功")
            {
                $("#picId"+picId+" textarea").attr("title",remark);
            }else {

            }

        }
    });


}
function copyurl(picId) {
    var host = $("#contextPath").attr("href");
    var url = $("#picId"+picId+" .picimg img").attr("src");
    var lay_url = $("#picId"+picId+" .picimg img").attr("lay-src");
    if (typeof(url)=="undefined")
    {
        layer.alert("请复制图片地址："+host+lay_url);
    }else {
        layer.alert("请复制图片地址："+host+url);
    }
    //alert(host+url);
}

function deletepic(picId) {

    $.ajax({
        url:"/deletepic/"+picId,
        cache:false,
        //async: false,
        type:"post",
        success:function (msg) {
            layer.msg("删除成功！");
            $("#picId"+picId).remove();
        }
    });
}

function changeselect(picId) {
    // alert(picId);
     var selectValue = $("#selectid"+picId).val();
    // alert("选择权限:"+selectValue);
    $.ajax({
        url:"/changepicstatus/"+picId+"/"+selectValue,
        cache:false,
        //async: false,
        type:"post",
        success:function (msg) {
            layer.msg(msg);

        }
    });
}

function changeSelectListener(id) {
    alert("id:"+id);
    layui.from.on('select('+id+')', function(data){
        alert(data.value);
    });

}