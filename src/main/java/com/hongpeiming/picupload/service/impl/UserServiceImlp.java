package com.hongpeiming.picupload.service.impl;

import com.hongpeiming.picupload.mapper.UserMapper;
import com.hongpeiming.picupload.pojo.User;
import com.hongpeiming.picupload.pojo.UserExample;
import com.hongpeiming.picupload.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserServiceImlp implements UserService {
    @Autowired
    UserMapper userMapper;

    @Override
    public User selectUserById(Integer userId) {
        try {
            User user = userMapper.selectByPrimaryKey(userId);
            System.out.println(user.getUserAccount());
            return user;
        }catch (Exception e)
        {
            System.out.println("找不到ID为"+userId+"的用户！");
            return null;
        }

    }

    @Override
    public User selectUserByAccount(String username) {
        UserExample example = new UserExample();
        UserExample.Criteria criteria = example.createCriteria();
        criteria.andUserAccountEqualTo(username);
        List<User> users = userMapper.selectByExample(example);
        return users.get(0);
    }

    @Override
    public boolean checkUserAccountRepeated(String UserAccount) {
        UserExample example = new UserExample();
        UserExample.Criteria criteria = example.createCriteria();
        criteria.andUserAccountEqualTo(UserAccount);
        List<User> users = userMapper.selectByExample(example);
        if (users.size()!=0)
            return true;
        else
            return false;
    }

    @Override
    public int register(User user) {
        int userId;
        try {
            userId = userMapper.insert(user);
            System.out.println("刚注册的账号ID"+userId);
        }catch (Exception e) {
            return 0;
        }
        return userId;
    }

    @Override
    public boolean checkUserPass(String userAccount, String password) {
        try {
            UserExample example = new UserExample();
            UserExample.Criteria criteria = example.createCriteria();
            criteria.andUserAccountEqualTo(userAccount);
            List<User> users = userMapper.selectByExample(example);
            User user = users.get(0);
            if (user.getUserPassword().equals(password))
            {
                return true;
            }

        }catch (Exception e)
        {
            System.out.println("找不到此用户");
        }

        return false;
    }
}
